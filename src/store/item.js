import axios from 'axios'


export default{
    /* eslint-disable no-console */
    state: {
        books: null,
        itemData: [],
        chosenData: [],
        count: 0,
        totalBalance: null,
        successOrder: false,
        orderError: false,
        orders: null,
        pageData: null,
    },
    mutations: {
        itemTaker(state,payload){
            state.books = payload.data.payload
        },
        itemDataTaker(state,payload){
            localStorage.setItem("basket", JSON.stringify(payload))
            state.itemData = payload
            var c = 0;
            var totalPrice = 0;
            for(var i = 0;i < payload.length;i++){
                c += payload[i].quantity;
                totalPrice += payload[i].price * payload[i].quantity;
            }
            state.totalBalance = totalPrice
            state.count = c
            localStorage.setItem("count", JSON.stringify(c))
            localStorage.setItem("totalBalance", JSON.stringify(totalPrice))
        },
        chosenDataTaker(state,payload){
            state.chosenData = payload
            localStorage.setItem("chosen", JSON.stringify(payload))
        },
        changeSuccess(state){
            state.successOrder = true;
            state.orderError = false;
        },
        cleanBasket(state){
            localStorage.removeItem('basket');
            localStorage.removeItem('count');
            localStorage.removeItem('totalBalance');
            state.itemData = []
            state.count = 0
            state.totalBalance = null
        },
        changeError(state){
            state.successOrder = false;
            state.orderError = true;
        },
        getOrders(state,payload){
            state.orders = payload.data.payload
            state.pageData = payload.data.page_data
            state.successOrder = false
            var total = 0;
            for(var i = 0;i < payload.data.payload.length;i++){
                for(var k = 0;k < payload.data.payload[i].order_books.length;k++){
                    total += payload.data.payload[i].order_books[k].price * payload.data.payload[i].order_books[k].count
                    payload.data.payload[i].order_books[k].itemTotal = total
                    }
                    payload.data.payload[i].totalPrice = total
                    total = 0
            }
            for(var a = 0;a < payload.data.payload.length;a++){
                for(var j = 0;j < payload.data.payload[a].order_books.length;j++){
                    total += payload.data.payload[a].order_books[j].price * payload.data.payload[a].order_books[j].count
                    payload.data.payload[a].order_books[j].itemTotal = total
                    total = 0
                    }
            }
        },
        getLocalStorage(state){
            state.itemData = JSON.parse(localStorage.getItem('basket')) ? JSON.parse(localStorage.getItem('basket')) : [],
            state.chosenData = JSON.parse(localStorage.getItem('chosen')) ? JSON.parse(localStorage.getItem('chosen')) : [],
            state.count = JSON.parse(localStorage.getItem('count')) ? JSON.parse(localStorage.getItem('count')) : 0,
            state.totalBalance = JSON.parse(localStorage.getItem('totalBalance')) ? JSON.parse(localStorage.getItem('totalBalance')) : null
        }

    },
    actions: {
        getItem(  {commit} ){ 
            axios.get('http://shop.test/api/books')
                
            .then((response) =>{
                commit('itemTaker',response)
            })
          },
        itemsData(  {commit} , payload){ 
              commit('itemDataTaker' , payload)  
          },
        chosenData(  {commit} , payload){ 
            commit('chosenDataTaker' , payload)  
        },
        addComment(  {dispatch} , comment){ 
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
              }
            axios.post('http://shop.test/api/book-rating', comment,{headers: headers})
                
            .then(() =>{
                // handle success
                dispatch('getItem')
            })
            .catch(function () {
                // handle error
              })
          },
        sendOrder(  {commit} , order){ 
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
              }
            axios.post('http://shop.test/api/create-order', order,{headers: headers})
                
            .then((response) =>{
                // handle success
                if(response.data.success){
                    commit('changeSuccess')
                    commit('cleanBasket')
                }else(
                    commit('changeError')
                )
            })
            .catch(function () {
                // handle error
                commit('changeError')
              })
          },
        getStory(  {commit} , payload){ 
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
              }

            const page = payload

            axios.get('http://shop.test/api/ordered-book?page=' + page,{headers: headers})
                
            .then((response) =>{
                console.log(response)
               commit('getOrders',response)
            })
            .catch(function () {
                // handle error
                commit()
              })
          },
        getLocalStorage({ commit }){
            commit('getLocalStorage')
        }
    },
    getters: {
        books(state){
            return state.books
        },
        itemData(state){
            return state.itemData
        },
        chosenData(state){
            return state.chosenData
        },
        count(state){
            return state.count
        },
        totalBalance(state){
            return state.totalBalance
        },
        successOrder(state){
            return state.successOrder
        },
        orderError(state){
            return state.orderError
        },
        orders(state){
            return state.orders
        },
        pageData(state){
            return state.pageData
        },
    }
}