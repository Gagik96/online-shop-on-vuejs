import axios from 'axios'


export default{
    state: {
        user: null,
        userName: null,
        token: null,
        isSuccess: null,
        error: false,
    },
    mutations: {
        /* eslint-disable no-console */
        signUser(state,payload){
            state.user = payload;
            localStorage.setItem('token', JSON.stringify(payload.userData.token));
            localStorage.setItem('name', JSON.stringify(payload.userData.name));
            state.isSuccess = true;
            state.token = payload.userData.token;
            state.userName = payload.userData.name;
            state.error = false;
        },
        logoutUser(state){
            state.isSuccess = false;
            var auth = false;
            localStorage.setItem('isSuccess', JSON.stringify(auth));
            state.user = null;
            state.token = null;
            state.userName = null;
        },
        errorFind(state){
            state.error = true;
        },
        getLocalStorage(state){
            state.userName = JSON.parse(localStorage.getItem('name')) && JSON.parse(localStorage.getItem('name'))!=='undefined' ? JSON.parse(localStorage.getItem('name')) : null,
            state.token = JSON.parse(localStorage.getItem('token')) && JSON.parse(localStorage.getItem('token'))!=='undefined'  ? JSON.parse(localStorage.getItem('token')) : null,
            state.goToOrder = JSON.parse(localStorage.getItem('goToOrder'))
        },

    },
    actions: {
        /* eslint-disable no-console */
        
        addUser(  {commit} , body){ 
            axios.post('http://shop.test/api/register', body)
            
            .then((response) =>{
            // handle success
                if(response.data.success){
                    let userData = {
                        name: response.data.payload.name,
                        token: response.data.payload.token,
                        email: response.data.payload.email,
                        id: response.data.payload.id,
                    }
                    commit('signUser',{ userData })
                }else{
                    commit('errorFind')
                }
            })
            .catch(function () {
                // handle error
                commit('errorFind')
            })
      },
      userRequest(  {commit} , user){ 
        axios.post('http://shop.test/api/login', user)
            
        .then((response) =>{
            // handle success
            if(response.data.success){
                let userData = {
                    name: response.data.payload.name,
                    token: response.data.payload.token,
                    email: response.data.payload.email,
                    id: response.data.payload.id,
                }
                commit('signUser',{ userData })
            }else{
                commit('errorFind')
            }
             
        })
        .catch(function () {
            // handle error
            commit('errorFind')
          })
        
    },
      logoutRequest({commit}){
        axios.get('http://shop.test/api/logout')
        .then((response) =>{
            // handle success
            if(response.statusText == 'OK'){
                localStorage.removeItem('token');
                localStorage.removeItem('name');
                commit('logoutUser')
            }
             
        })
      },
      getLocalStorage({ commit }){
          commit('getLocalStorage')
      }
    },
    getters: {
        users(state){
            return state.user
        },
        token(state){
            return state.token
        },
        isSuccess(state){
            return state.isSuccess
        },
        userName(state){
            return state.userName
        },
        error(state){
            return state.error
        },
    },
}