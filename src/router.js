import Vue from 'vue'
import Router from 'vue-router'
import appliances from '@/views/catalog/appliances'
import smartphones from '@/views/catalog/smartphones'
import TVAndEntertainment from '@/views/catalog/TVAndEntertainment'
import computers from '@/views/catalog/computers'
import officeAndNetwork from '@/views/catalog/officeAndNetwork'
import accessories from '@/views/catalog/accessories'
import carsProducts from '@/views/catalog/carsProducts'
import instruments from '@/views/catalog/instruments'
import services from '@/views/catalog/services'
import discountedGoods from '@/views/catalog/discountedGoods'
import Login from '@/views/auth/Login'
import Order from '@/views/orders/Order'
import Story from '@/views/orders/Story'
import Home from '@/views/Home'
import Registration from '@/views/auth/Registration'
import basket from '@/views/basket/basket'
import chosen from '@/views/chosen/chosen'

Vue.use(Router)

export default new Router({
    mode:'history',
    base: process.env.BASE_URL,
    routes: [
    {
        path: '/login',
        component: Login,
        meta: {layout: 'out'}
    },
    {
        path: '/registration',
        component: Registration,
        meta: {layout: 'out'}
    },
    {
        path: '/order',
        component: Order,
        meta: {layout: 'main'}
    },
    {
        path: '/story',
        component: Story,
        meta: {layout: 'main'}
    },
    {
        path: '/',
        component: Home,
        meta: {layout: 'main'}
    },
    {
        path: '/appliances',
        component: appliances,
        meta: {layout: 'main'}
    },
    {
        path: '/smartphones',
        component: smartphones,
        meta: {layout: 'main'}
    },
    {
        path: '/TV-and-entertainment',
        component: TVAndEntertainment,
        meta: {layout: 'main'}
    },
    {
        path: '/computers',
        component: computers,
        meta: {layout: 'main'}
    },
    {
        path: '/office-and-network',
        component: officeAndNetwork,
        meta: {layout: 'main'}
    },
    {
        path: '/accessories',
        component: accessories,
        meta: {layout: 'main'}
    },
    {
        path: '/cars-products',
        component: carsProducts,
        meta: {layout: 'main'}
    },
    {
        path: '/instruments',
        component: instruments,
        meta: {layout: 'main'}
    },
    {
        path: '/services',
        component: services,
        meta: {layout: 'main'}
    },
    {
        path: '/discounted-goods',
        component: discountedGoods,
        meta: {layout: 'main'}
    },
    {
        path: '/basket',
        component: basket,
        meta: {layout: 'main'}
    },
    {
        path: '/chosen',
        component: chosen,
        meta: {layout: 'main'}
    },
]
})